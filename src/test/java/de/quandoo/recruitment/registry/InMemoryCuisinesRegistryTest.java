package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.*;
import org.junit.*;

//TODO: Change test method names for something more meaningful!!
public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void shouldWork1() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        cuisinesRegistry.cuisineCustomers(new Cuisine("french")); //FIXME: Where is the assertion??
    }

    @Test
    public void shouldWork2() {
        //FIXME: Where is the assertion??
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test
    public void shouldWork3() {
        //FIXME: Where is the assertion??
        cuisinesRegistry.customerCuisines(null);
    }

    @Test(expected = RuntimeException.class)
    public void thisDoesntWorkYet() {
        cuisinesRegistry.topCuisines(1);
    }


}