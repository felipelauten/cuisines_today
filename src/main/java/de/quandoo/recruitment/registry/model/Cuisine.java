package de.quandoo.recruitment.registry.model;

public class Cuisine {

    private final String name;

    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Cuisine cuisine = (Cuisine) o;

        return name.equals(cuisine.name);
    }

    @Override public int hashCode() {
        return name.hashCode();
    }

    @Override public String toString() {
        return "Cuisine{" + "name='" + name + '\'' + '}';
    }
}
