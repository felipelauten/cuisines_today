package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.*;
import de.quandoo.recruitment.registry.model.*;
import java.util.*;
import java.util.AbstractMap.*;
import java.util.Map.*;
import java.util.stream.*;

/* TODO: Our famous rather inexperienced developer made some design decisions, lets say, quite inefficient.
   We have serious limitations and BUGS in this class. To fully have the Book-That-Table's requirements met, this has to be changed! Here is why:
   1. The main feature of this class, the register method, is NOT WORKING. To identify the right List to put the customer in, the if checks the cuisineName
      with == rather than with an equals method. The outcome of this is that for every call we will fail because == compare memory addresses of the objects,
      not their content. This can be verified by running the de.quandoo.recruitment.registry.InMemoryCuisinesRegistryTest#shouldWork1, it print's the error
      message every time it is called. Basically the lists won't keep track of any Customer! Serious bug, OMG!!!
   2. The current implementation doesn't use Generics, if another inexperienced developer change the implementation and tries to put an object of a different
      type in these Lists we will have RuntimeException's!
   3. Our service cannot grow easily! If we need to keep track of more types of cuisines, than for every new cuisine we will have to implement new LinkedLists
      to manage the problem. A better solution is to have another data structure to map the customers for the specific cuisine. Using a Map with Lists of
      customers we will be able to correctly handle the growing number of cuisine types our service support without changing the application's code.
   4. By using Lists, the methods register, cuisinesCustomers and customerCuisines need to have if statements with hard-coded strings to find the correct
      information. Again, using the Map implementation will be simpler.
   5. On the method register, we have an ugly System.err message, the CuisinesRepository need to support new cuisines types. Apart of that, it would be better
      to use some logging framework. When we deploy our service to an application service this will be an issue because we cannot keep track of the application
      errors in a proper way. Let's configure it, simple and it will allow us to have log files to send to the support team!
   6. Top cuisines method wasn't implemented and throws an runtime error!! This is against the SOLID principles and if somebody call this method in the current
      version of the software the support team will have a bad day! This shouldn't be here, OMG! =(
   7. The register method is not thread-safe, potential problem with concurrency!
   8. On cuisineCustomers(..) we should return a copy of the lists, somebody can mutate the data structure leading to bugs hard to spot!!!
*/
public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<Cuisine, List<Customer>> customersByCuisine = new HashMap<>();

    @Override
    public synchronized void register(final Customer customer, final Cuisine cuisine) {
        List<Customer> customerList = customersByCuisine.get(cuisine);

        if (customerList == null) {
            customerList = new LinkedList<>();
        }
        customerList.add(customer);

        customersByCuisine.put(cuisine, customerList);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if (cuisine == null) {
            return Collections.emptyList();
        }

        List<Customer> customerList = customersByCuisine.get(cuisine);
        if (customerList == null) {
            return Collections.emptyList();
        }

        return new LinkedList<>(customerList);
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        if (customer == null) {
            return Collections.emptyList();
        }

        final List<Cuisine> customerCuisines = new ArrayList<>();
        for (Entry<Cuisine, List<Customer>> entry : customersByCuisine.entrySet()) {
            if (entry.getValue().contains(customer)) {
                customerCuisines.add(entry.getKey());
            }
        }

        return customerCuisines;
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        if (customersByCuisine.isEmpty()) {
            return Collections.emptyList();
        }

        List<Cuisine> topCuisines;
        topCuisines = customersByCuisine.keySet().stream()
                                        .map(cuisine -> new SimpleEntry<>(cuisine, customersByCuisine.get(cuisine).size()))
                                        .sorted(InMemoryCuisinesRegistry::topCuisinesReversedComparator)
                                        .map(entry -> entry.getKey())
                                        .collect(Collectors.toList());

        return new LinkedList<>(topCuisines.subList(0, n));
    }

    private static int topCuisinesReversedComparator(Entry<Cuisine, Integer> left, Entry<Cuisine, Integer> right) {
        int leftCount  = left.getValue();
        int rightCount = right.getValue();

        if (leftCount < rightCount) {
            return 1;
        } else if (leftCount > rightCount) {
            return -1;
        } else {
            return 0;
        }
    }
}
